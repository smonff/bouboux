requires 'Data::Printer', '0';
requires 'DBD::SQLite', '1.62';
requires 'DBIx::Class', '0.082841';
requires 'DBIx::Class::Schema::Loader', '0.07049';
requires 'Mojolicious', '8.17';
requires 'Mojo::SQLite', '3.001';
requires 'Yancy', '1.031';
