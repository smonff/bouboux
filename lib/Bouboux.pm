package Bouboux;
use Mojo::Base 'Mojolicious';
use Data::Printer;
use Bouboux::Model::Schema;

# This method will run once at server start
sub startup {
  my $self = shift;

  # Load configuration from hash returned by config file
  my $config = $self->plugin('Config');

  has schema => sub {
    my $connection_params = build_datasource( $config );
    return Bouboux::Model::Schema->connect(
      $connection_params->{data_source},
      $connection_params->{username},
      $connection_params->{password},
      $connection_params->{options});
  };

  $self->helper(db => sub { $self->app->schema });

  $self->plugin(
    Yancy => {
      backend => 'sqlite:' . $config->{db}->{path},
      read_schema => 1,
      schema => {
        plant => {
          'x-list-columns' => [
            qw/ latin_name common_name family_id validated /
         ],
        },
        family => {
          'x-list-columns' => [  'id', 'name' ],
        },
        person => {
          'x-list-columns' => [  qw/ email is_admin / ],
        },
        give => {
          'x-ignore' => 1
        },
        seek => {
          'x-ignore' => 1
        }
      }
    });

  $self->yancy->plugin(
    'Auth::Password' => {
      schema => 'admin',
      username_field => 'email',
      password_field => 'password',
      password_digest => {
        type => 'SHA-512',
      },
    });

  # Configure the application
  $self->secrets($config->{secrets});

  # Router
  my $r = $self->routes;

  # Normal route to controller
  $r->get('/')->to('example#welcome');
  $r->get('/hash/:pass')->to('password#hash');
}

sub build_datasource ( ) {
  my $config = shift;
  my $data_source = 'dbi:SQLite:' . $config->{db}->{path};
  my $params = {
    data_source => $data_source,
    options => { RaiseError => 1 }
  };
  return $params;
}


1;
