package Bouboux::Controller::Password;
use Mojo::Base 'Mojolicious::Controller',  -signatures;
use Term::ANSIColor qw( :constants );
use Digest::SHA qw(sha512_base64);

=head2 Creating a hashed password for a user

  GET /pass/hash/:name/:pass

Copy paste the result from the logs to the database for creating users. If you
put the value in JSON, some problems could happend with escaped '/'

=cut
# This action will render a template
sub hash ($c) {
  my $digested_pass = sha512_base64( $c->param('pass') );
  say 'The password is ', BOLD, MAGENTA, $digested_pass, RESET;
  $c->render(json => {
    'sha-512' => 'Copy paste the result from the logs into the database'
  });
}

1;
