package Bouboux::Controller::Example;
use Mojo::Base 'Mojolicious::Controller',  -signatures;
use Data::Printer;

# This action will render a template
sub welcome ($c) {

  p $c->param('family');

  my (@plants, @families) = ();

  my $family_rs =  $c->app->db->resultset('Family')->search(
    {},
    { order_by => {
      -asc => 'name'
     }});

  while (my $family = $family_rs->next ) {
    push @families, {
      id => $family->id,
      name => $family->name
     };
  }

  my $plants_rs =  $c->app->db->resultset('Plant')->search();

  while (my $plant = $plants_rs->next ) {
    push @plants, {
      latin => $plant->latin_name,
      family => $plant->family->name,
      id => $plant->family->id
     };
    }

  my @filtered_plants = grep {
    say $c->param('family');
    $c->param('family') ? $_->{id} == $c->param('family') : $_;
  } @plants;

  p @filtered_plants;

  # Render template "example/welcome.html.ep" with message
  $c->render(
    title => 'Bouboux',
    msg => 'Welcome to Bouboux!',
    plants => \@filtered_plants,
    families => \@families,
   );
}

1;
