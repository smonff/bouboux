use utf8;
package Bouboux::Model::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-06-08 21:11:21
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:GomG3O1gTBUTv7l2KSxS5Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
