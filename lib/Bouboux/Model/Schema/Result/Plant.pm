use utf8;
package Bouboux::Model::Schema::Result::Plant;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Bouboux::Model::Schema::Result::Plant

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<plant>

=cut

__PACKAGE__->table("plant");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 latin_name

  data_type: 'text'
  is_nullable: 0

=head2 common_name

  data_type: 'text'
  is_nullable: 1

=head2 family_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 validated

  data_type: 'boolean'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "latin_name",
  { data_type => "text", is_nullable => 0 },
  "common_name",
  { data_type => "text", is_nullable => 1 },
  "family_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "validated",
  { data_type => "boolean", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<latin_name_unique>

=over 4

=item * L</latin_name>

=back

=cut

__PACKAGE__->add_unique_constraint("latin_name_unique", ["latin_name"]);

=head1 RELATIONS

=head2 family

Type: belongs_to

Related object: L<Bouboux::Model::Schema::Result::Family>

=cut

__PACKAGE__->belongs_to(
  "family",
  "Bouboux::Model::Schema::Result::Family",
  { id => "family_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-06-10 21:22:53
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:CrERHbloZKViczfbZXRsNw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
