use utf8;
package Bouboux::Model::Schema::Result::Seek;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Bouboux::Model::Schema::Result::Seek

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<seek>

=cut

__PACKAGE__->table("seek");

=head1 ACCESSORS

=head2 plant_id

  data_type: 'int'
  is_nullable: 1

=head2 person_id

  data_type: 'int'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "plant_id",
  { data_type => "int", is_nullable => 1 },
  "person_id",
  { data_type => "int", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-06-08 21:11:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:CqqI8adEk+U0QeLT7Hyfew


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
