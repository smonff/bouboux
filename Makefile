CARTON_EXE=carton exec --

perl-install:
	carton install

make morbo-run:
	DBI_TRACE=0 $(CARTON_EXE) morbo script/bouboux

generate-schema:
	${CARTON_EXE} dbicdump \
	-o dump_directory=./lib/ \
	-o components='["InflateColumn::DateTime"]' \
	-o preserve-case=1 \
	Bouboux::Model::Schema dbi:SQLite:./data/data_bouboux.db

db-dump:
	sqlite3 data/data_bouboux.db .dump > data/dump.sql
