PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE family
(
	id integer
		constraint family_pk
			primary key autoincrement,
	name text not null
);
INSERT INTO family VALUES(1,'Cactaceae');
INSERT INTO family VALUES(2,'Araceae');
INSERT INTO family VALUES(3,'Begoniaceae');
INSERT INTO family VALUES(4,'Pteridaceae');
INSERT INTO family VALUES(5,'Asclepiadaceae');
INSERT INTO family VALUES(6,replace('Arecaceae\n','\n',char(10)));
INSERT INTO family VALUES(7,'Liliaceae');
INSERT INTO family VALUES(8,'Agavaceae');
INSERT INTO family VALUES(9,'Moraceae');
INSERT INTO family VALUES(10,'Apocynaceae');
INSERT INTO family VALUES(11,'Dryopteridaceae');
INSERT INTO family VALUES(12,'Urticaceae');
INSERT INTO family VALUES(13,'Asparagaceae');
CREATE TABLE seek
(
	plant_id int,
	person_id int
);
CREATE TABLE give
(
	plant_id int,
	person_id int
);
CREATE TABLE IF NOT EXISTS "plant"
(
	id integer not null
		constraint plant_pk
			primary key autoincrement,
	latin_name text not null,
	common_name text,
	family_id integer not null
		references family,
	validated boolean not null
);
INSERT INTO plant VALUES(2,'Monstera deliciosa','Faux philodendron',2,1);
INSERT INTO plant VALUES(3,'Begonia maculata wightii','Polka dot begonia',3,1);
INSERT INTO plant VALUES(4,'Nephrolepis exaltata','Néphrolepis',11,1);
INSERT INTO plant VALUES(5,'Adiantum capillus-veneris','Capillaire',4,1);
INSERT INTO plant VALUES(6,'Begonia maculata','Begonia Tamaya',3,1);
INSERT INTO plant VALUES(7,'Scindapsus aureus','Pothos',2,1);
INSERT INTO plant VALUES(8,'Ficus benjamina ','Ficus',9,1);
INSERT INTO plant VALUES(9,'Pilea peperomioides','Plante du missionnaire',12,1);
INSERT INTO plant VALUES(10,'Chamaedorea elegans','Palmier nain',6,1);
INSERT INTO plant VALUES(11,'Chlorophytum comosum','plante araignée',7,1);
INSERT INTO plant VALUES(12,'Sansevieria trifasciata','Langue de belle-mère,Couteau',13,1);
INSERT INTO plant VALUES(13,'Begonia rex','Bégonia Rex',3,1);
INSERT INTO plant VALUES(14,'Dracaena marginata','Dracaena',8,1);
INSERT INTO plant VALUES(15,'Begonia erythrophylla','Bégonia Nénuphar',3,1);
INSERT INTO plant VALUES(16,'Caladium bicolor','Caladium ‘White Queen’',2,1);
INSERT INTO plant VALUES(17,'Begonia conchifolia rubrimacula','Begonia Red Ruby',3,1);
INSERT INTO plant VALUES(18,'Pilea glauca','Pilea glauque',12,1);
INSERT INTO plant VALUES(19,'Ceropegia woodii','Chaîne des cœurs',5,1);
INSERT INTO plant VALUES(20,'Rhipsalis','Rhipsalis',1,1);
INSERT INTO plant VALUES(21,'Scindapsus pictus Argyraeus','Scindapsus pictus',2,1);
INSERT INTO plant VALUES(22,'Hoya kerrii','Hoya',10,1);
INSERT INTO plant VALUES(23,'Epiphyllum anguliger','Cactus zigzag',1,1);
CREATE TABLE IF NOT EXISTS "person"
(
	id integer not null
		constraint person_pk
			primary key autoincrement,
	email text not null,
	password text not null,
	is_admin int default 0 not null
);

CREATE TABLE admin
(
	id integer not null
		constraint admin_pk
			primary key autoincrement,
	email text not null,
	password text
);

DELETE FROM sqlite_sequence;
INSERT INTO sqlite_sequence VALUES('family',13);
INSERT INTO sqlite_sequence VALUES('plant',23);
INSERT INTO sqlite_sequence VALUES('person',3);
INSERT INTO sqlite_sequence VALUES('admin',2);
CREATE UNIQUE INDEX family_id_uindex
	on family (id);
CREATE UNIQUE INDEX family_name_uindex
	on family (name);
CREATE UNIQUE INDEX plant_latin_name_uindex
	on plant (latin_name);
CREATE UNIQUE INDEX person_email_uindex
	on person (email);
CREATE UNIQUE INDEX admin_id_uindex
	on admin (id);
COMMIT;
