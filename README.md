# the-plant-exchange

(temporary name)

Ideas for the name of the app:

PP-TROC
PLANT-X
PEXY
BOUTURE
BOUBOUX
GREEN IN
VERTROC
PLANTSWAPPING
PROPAGO (from propagation)

SLOGAN IDEAS:
English
Green thumbs up
Help your plants take over the world.

French
D’une plante á mille plantes
Une plante fait mille plantes


ESSENTIAL IDEA and MUST HAVE FEATURES:
It’s an app for exchanging house plants. There’s a database that contains the latin names of common houseplants. People enter the plants/cuttings they have available for exchange (with autocomplete from database) and the plants they would like to have. The program matches them with corresponding profiles. (Similar to Conversation Exchange profile-matching). Then the users message each other to arrange the exchange.

BRAINSTORMING ABOUT FEATURES:

+ Should the database contain common names of plants in addition to the latin names? 
+ It can start for only in and around Paris, later we can have a map feature that makes it possible to mark location.
+ If the plant the person wants to enter does not exist in the database, we can have a link through which people can suggest new entries in the database. The new entries would become a part of the database only after approval by the admin.
+ It should be possible to browse profiles according to plants for giving or taking so that the seeker can strategise his/her exchanges to get the plant they want. For example if I have a monstera deliciosa and I’m looking for a pilea but the only person who has pilea wants a ceropegia, I can exhange my monstera against ceropagia, later I can take a cutting from my ceropegia to make the exchange I wanted in the first place.
